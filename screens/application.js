
(function(){
    'use strict';
    var app = angular.module('novacatApp', [ 'ngSanitize', 'ngCookies' ]);

})();

(function() {
    'use strict';

    var dataService = function($http, $q) {
        this.$http = $http;
        this.$q = $q;
    };

    dataService.$inject = ['$http', '$q'];


    dataService.prototype.getApplication  = function( pid ) {
        var _this = this;
        return _this.$http.get('//sherman.library.nova.edu/api/novapoint.php?job=novaCatApplication&pid='+pid)
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);

                return _this.$q.reject(response.data);
            })
    };

	 dataService.prototype.getAccount  = function() {
        var _this = this;
        return _this.$http.get('//sherman.library.nova.edu/api/novapoint.php?job=getAccount')
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    //console.log(response.data);
                    return response.data;
                    // invalid response
                    //return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                //console.log(response);
                return _this.$q.reject(response.data);
            })
    };

	  dataService.prototype.updateEmail  = function( params ) {
        var _this = this;
        return _this.$http.get('//sherman.library.nova.edu/api/novapoint.php?job='+params.job+'&email='+params.email+'&pid='+params.pid )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };

	 dataService.prototype.updateHomeLibrary  = function( params ) {
        var _this = this;
        return _this.$http.get('//sherman.library.nova.edu/api/novapoint.php?job='+params.job+'&field='+params.field+'& locx00='+params.locx00+'&pid='+params.pid )
            .then(function(response){
                if (typeof response.data === 'object') {
                    //console.log(response.data);
                    return response.data;
                } else {
                    // invalid response
                    return _this.$q.reject(response.data);
                }

            }, function(response) {
                // something went wrong
                return _this.$q.reject(response.data);
            })
    };


    angular.module('novacatApp')
        .service('dataService', dataService);
}());

(function(){

    'use strict';

    var ApplicationController = function( $rootScope, $window, $location,  $cookies, $interval, dataService){

    var vm 			= this;
	//var vm.pid;


	if($cookies.get( 'pid' )){
		vm.pid = $cookies.get( 'pid' );
	} else {
		vm.pid			= 'public';
		console.log('not logged');
	}

	vm.col = 'n';
	vm.libraries = [
		{ code: 'main', label: 'Alvin Sherman Library'},
		{ code: 'nmb', label: 'North Miami Beach'},
		{ code: 'ocean', label: 'Oceanographic Center Library'}
	];

        function getApplication(){
            dataService.getApplication(vm.pid).then(function(data){

                if(data) {

                  if ( data.response.AppStatus ) {
                   // vm.hours  = data.response.hours;

                      vm.status = data.response.AppStatus;

                      if(vm.status !== 'public'){
                          vm.info             = data.response.info;
                          vm.email_update     = data.response.info.email;
                          vm.activity             =    data.response.activity;
                          vm.col              = vm.info.col;
                          vm.selectedLibrary =  vm.info.home_lib;
                      }
                   }
                }
            });
        }

        // Does this need to be conditional on toplogo__loggedin?
        getApplication();

        var userMenuToggle = function(){
            $rootScope.display =   $rootScope.display == 'none' ? 'block' : 'none';
        };

        var showModalWindow = function(object){
            object.show = true;
            vm.modal = object;
        };

        var closeModalWindow = function(){
            vm.modal = {};
        };

        var updateEmail = function(){
            console.log(vm.email_update);
            var object = {
                job: 'updateEmail',
                email: vm.email_update,
				pid: vm.pid
            };
            console.log(object);
            dataService.updateEmail(object).then(function(data){
                console.log(data);

                if(data.update_error){
                    vm.email_update =  vm.info.email;
                    vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){
                    vm.email_update = data.response;
                    vm.info.email = data.response;
                    vm.update_message = '<div class="alert alert--success">Email updated!</div>';
                }

            });
        };

        var updateHomeLibrary = function(){

            var object = {
                job: 'updateHomeLibrary',
                field: 'fixedFields',
                locx00 : vm.selectedLibrary,
				pid: vm.pid

            };

            dataService.updateHomeLibrary(object).then(function(data){
                console.log(data);

                if(data.update_error){
                   vm.update_message = '<div class="alert alert--warning">' + data.update_error + '</div>';
                }

                if(data.response){

                     vm.info.home_lib = data.response;
                     vm.update_message = '<div class="alert alert--success">Home Library updated!</div>';
                }

            });

        };

        var logout = function(){

            $cookies.remove( 'pid' );
             $window.location = 'https://sherman.library.nova.edu/auth/extlogout/novacatout.php';
        };

        $rootScope.userMenuToggle       = userMenuToggle;
        vm.showModalWindow              = showModalWindow;
        vm.closeModalWindow  = closeModalWindow;
        vm.updateEmail       = updateEmail;
        vm.updateHomeLibrary = updateHomeLibrary;
        vm.logout            = logout;
    };

    ApplicationController .$inject = [ '$rootScope', '$window', '$location', '$cookies', '$interval', 'dataService'];

    angular.module('novacatApp')
        .controller('ApplicationController', ApplicationController);

})();


(function(){

    'use strict';

    var AccountController = function( dataService ){

        var vm    = this;
        vm.dbAcc  = false;
        vm.subAcc = false;

        function  getAccount(){
            dataService.getAccount().then(function(data){
                //console.log(data);
                if(data) {
                    vm.databases  = data.response.databases;
                    vm.subjects   = data.response.subjects;
                }
            });
        }

        getAccount();

        var dbAccToggle = function(){
            vm.dbAcc =  vm.dbAcc == false ? true : false;
        };

        var subAccToggle = function(){
            vm.subAcc = vm.subAcc == false ? true : false;
        };

        var startsWith = function (actual, expected) {
            var lowerStr = (actual + "").toLowerCase();
            return lowerStr.indexOf(expected.toLowerCase()) === 0;
        };

        vm.dbAccToggle  = dbAccToggle;
        vm.subAccToggle = subAccToggle;
        vm.startsWith   = startsWith;
    };

    AccountController.$inject = ['dataService'];

    angular.module('novacatApp')
        .controller('AccountController', AccountController);

})();
