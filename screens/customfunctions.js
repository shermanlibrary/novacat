$(document).ready(function() {
    $('#SEARCH').focus();
});

function loggedMessage(){
    //function loggedMessage - rewritten JMH 6/11/14
    //set variables
    var message     = document.getElementById("logged_msg");
	if( message  != null)

    {
		var name        = document.getElementById("logged_name"),
		logged_msg      = message.innerHTML,
		logged_name     = name.innerHTML,
		openspan        = "<span class=\'pageMainAreaSubHeader logged_msg\'>Hello, ",
		closespan       = ", you are logged into NovaCat\.<\/span>",
		lastfirstmore   = logged_name.match(/(\w+)[\W\s]+(\w+)[\W\s]+(\w{2,})/ig),
		lastfirstmi     = logged_name.match(/(\w+)[\W\s]+(\w+)[\W\s]+(\w)/ig),
		lastfirst       = logged_name.match(/(\w+)[\W\s]+(\w+)/ig),
		onename         = logged_name.match(/(\w+)[\W\s]?/ig),
		pageContent     = document.querySelector('.pageContentInner'),
		newDiv          = document.createElement("div"),
		nameMatch       = '';

		// prep the divs
		message.style.display = 'none';
		newDiv.id             = 'new_msg';
		if(pageContent){
			pageContent.insertBefore(newDiv, pageContent.childNodes[3]);
		}
		//match and rewrite the logged msg
		//ToDo place a more efficient switch statement.
    
        if (lastfirstmore)
        {
            msg = openspan + logged_name.replace(/(\w+)[\W\s]+(\w+)[\W\s]+(\w{2,})/ig,  "$2 $3")+ closespan;
        }
        else if (lastfirstmi)
        {
            msg = openspan + logged_name.replace(/(\w+)[\W\s]+(\w+)[\W\s]+(\w{1})/ig,"$2")+ closespan;
        }
        else if (lastfirst)
        {
            msg = openspan + logged_name.replace(/(\w+)[\W\s]+(\w+)/ig, "$2")+ closespan;
        }
        else if (onename)
        {
            msg = openspan + logged_name.replace(/(\w+)[\W\s]+/ig, "$1") + closespan;
        }
        else
        {
            msg = openspan + "Library Guest" + closespan;
        }

        return newDiv.innerHTML = msg;
    } else { return false; }

}
function briefCitReady(){
    var X = (document.getElementsByName("briefcit_nsu")); // creates an array of briefcit (this document) instances as it appears on a browse search results display.
    var Y = X.length; // Y = the array length for each instance of briefcit.  Y is a unique ID# for each instance of briefcit in the browse display - essentially creating a counter with a different length each time briefcit is used
    var BriefcitDivs = X[Y-1].getElementsByTagName("div") // create an array of all the Div tags for the latest instance of briefcit.
    var entry_media = BriefcitDivs[0].innerHTML;
    var title = BriefcitDivs[1].innerHTML;
    var titleFull = BriefcitDivs[2].innerHTML;
    var author= BriefcitDivs[3].innerHTML;
    var pub= BriefcitDivs[4].innerHTML;
    var items= BriefcitDivs[4].innerHTML;
    var request= BriefcitDivs[6].innerHTML;
    var date= BriefcitDivs[7].innerHTML;
    var link= BriefcitDivs[8].innerHTML;
    var bibHasLinks = link.match(/http/);
    var note= BriefcitDivs[9].innerHTML;
    var row = BriefcitDivs[10].innerHTML;
    var bibLinks = BriefcitDivs[11].innerHTML;
    var illiad ="<a href='http://ezproxy.library.nova.edu/login?auth=quad&url=https://illiad.library.nova.edu/FNN/illiad.dll/OpenURL?sid=iii:NovaCatHolds&genre=book&isbn=";
    var htmlLink = "<a href='" + link + "'>" + note + "</a>";
    var illiadlink = (illiad + "&aulast=" + escape(author) + "&title=" + escape(titleFull) + "&date=" + escape(date) + "'><img src='/screens/requestILL.gif' border='0'></a>");
    var holdrow = ( request + " " + illiadlink);
    var holdable = request.match(/request\.gif/);

}

function truncateDescription(description,show,resource) {
    // split all the words and store it in an array
    var words = description.split(' '),
        new_description = '';

    // loop through each word
    for (i = 0; i < words.length; i++) {
        // process words that will visible to viewer
        if (i <= show) {
            new_description += words[i] + ' ';

            // process the rest of the words
        } else {

            // add a span at start
            if (i == (show + 1)) new_description += '... <span class="more_text hide">';
            new_description += words[i] + ' ';

            // close the span tag and add read more link in the very end
            if (words[i+1] == null) new_description += '</span><a href='+resource+' class="more_link"> &raquo; more</a>';
        }
    }
    return new_description;
}


function briefCitResource(){
    var X              = $('.brief-resource'), // creates an array of briefcit (this document) instances as it appears on a browse search results display.
        Y              = X.length, // Y = the array length for each instance of briefcit.  Y is a unique ID# for each instance of briefcit in the browse display - essentially creating a counter with a different length each time briefcit is used
        ResourceDivs   = X[Y-1].getElementsByTagName("div"), // create an array of all the Div tags for the latest instance of briefcit.
        entrynum 	   = ResourceDivs[0].innerHTML,
        access_link    = ResourceDivs[1].innerHTML,
        title	   = ResourceDivs[2].innerHTML,
        description    = ResourceDivs[3].innerHTML,
        access_icon    = ResourceDivs[4].innerHTML,
        access_icon	   = access_icon.match(/<.*>/g),
        resource_link  = ResourceDivs[5].innerHTML,
        resource_link  = resource_link.match(/"([^"]*")/g);
    var description = truncateDescription(description, 20, resource_link );

    ResourceDivs[6].innerHTML  = "<a href="+resource_link+">"+title+"</a>";
    ResourceDivs[7].innerHTML  = description;
    if(access_icon) {
        ResourceDivs[8].innerHTML  = access_icon;
    }
    ResourceDivs[9].innerHTML  = "<a href='" + access_link + "'>Connect to " + title + "</a>";
}





function resource_display() {
    var title 		= $('#resource_title td.resourceInfoData').text(),
        link_display 	= $('#resource_link_display'),
        link 			= $('#resource_link').html(),
        link 			= link.match(/"([^"]*")/g),
        public_note 	= $('#resource_note .resourceInfoData').html();
    $('#bibTitle').append(title);

    if(public_note) {
        link_display.append('Connect to <a href='+link+'>'+public_note+'</a>');

    }
    else {
        link_display.append('Connect to <a href='+link+'>'+title+'</a>');

    }
}

function showResourceSubject(){
    var subject = document.getElementById("subject").innerHTML;
    var subjectHit=/\w/gi;
    var subjectNote = subject.match(subjectHit);
    if (subjectNote != null){
        document.write('<div class="briefCiteSubj" style="margin-top:.25em"><strong>Subject:</strong> ' + subject + ' </div>');
    }
}

function topNavRow() {
    var navrow = document.getElementById("topNavRow").innerHTML;
    var stripnavrow = navrow.replace(/(<A href)/ig,"a href");
    var navrowlinks = stripnavrow.split("a href");
    var smsText = '<a href="#here" id="smsfeatures" name=smsbutton id=smsbutton onClick="showsms();return false;"><img src="/screens/smsbutton\.gif" border=0></a><br />';
    var requestIndex = -1;
    var addcartIndex = -1;
    var removecartIndex = -1;
    var divNavrowInnerHTML = "";
    for(i=0; i < navrowlinks.length; i++)
    {
        if(navrowlinks[i].match("request\.gif"))
        {
            //document.write("<a href" + navrowlinks[i]);
            requestIndex = i;
            //alert("request index"+i)
        }
        else if(navrowlinks[i].match("thisrecordsave\.gif"))
        {
            //document.write("<a href" + navrowlinks[i]);
            addcartIndex = i;
            //alert("addcart index"+i)
        }
        else if(navrowlinks[i].match("thisrecordremove\.gif"))
        {
            //document.write("<a href" + navrowlinks[i]);
            removecartIndex = i;
            //alert("removecart index"+i)
        }
        else
        {
            if(navrowlinks[i].replace(/^\s+|\s+$/g,"") != "")
            {
                divNavrowInnerHTML +="<a href" +  navrowlinks[i];
            }
            //alert("buttons"+i)
        }
    }

    document.getElementById("topNavRow").innerHTML = divNavrowInnerHTML;

    if(requestIndex != -1)
    {
        document.write("<a href" + navrowlinks[requestIndex] +  bibilliadlink + " " );
    }
    if(addcartIndex != -1)
    {
        document.write("<a href" + navrowlinks[addcartIndex] + smsText);
    }
    if(removecartIndex != -1)
    {
        document.write("<a href" + navrowlinks[removecartIndex] + smsText);
    }

    var f = document.getElementById('bib_detail');
    try {
        var itms = document.getElementById('bib_items');
        var tr = itms.getElementsByTagName('TR');
        for(i = 1; i < tr.length; i++) {
            var vmx=tr[i].getElementsByTagName('TD');
            if (vmx.length == 3) {
                var loc = vmx[0].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");
                var call = vmx[1].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");
                if (call.length < 1) {
                    call = 'n/a';
                }
                var status = vmx[2].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");
            }
        }

    } catch (e) {
        document.getElementById('smsfeatures').style.visibility='hidden';
    }

}

function patronView(){
    $(document).ready(function(){
        var emailAllertMsg = "<div style='width:80%;margin-top:20px;margin-left:auto;margin-right:auto;background: #fff;padding:10px;border:1px solid #eee'>It appears that you don't have an email or the supplied email address is not valid. You will need an email account to use many of our online services. Please see the listing below for free email providers.";
        emailAllertMsg += "<h3 style='text-align:center;font-weight:bold'>Free Email Providers</h3>";
        emailAllertMsg += "<p style='text-align:center'><a href='http://www.aol.com'>AOL</a> | <a href='http://mail.google.com'>Gmail</a> | ";
        emailAllertMsg += "<a href='http://www.hotmail.com'>Hotmail</a> | ";
        emailAllertMsg += "<a href='http://mail.yahoo.com/'>YahooMail</a> | <a href='http://www.emailaddresses.com/'>more</a><br />";
        emailAllertMsg += "In Spanish: <a href='http://mail.google.com/mail/?hl=es&amp;tab=wm'>Gmail</a> |&nbsp;";
        emailAllertMsg += "<a href='http://latino.msn.com'>Hotmail</a> | <a href='http://espanol.yahoo.com/r/tpco'>YahooMail</a> | ";
        emailAllertMsg += "<a href='http://www.emailaddresses.com/email_spanish.htm'>more</a> </p></div>";

        function trim(s){
            return rtrim(ltrim(s));
        }
        function ltrim(s){
            var l=0;
            while(l < s.length && s[l] == ' ')
            {	l++; }
            return s.substring(l, s.length);
        }
        function rtrim(s){

            var r=s.length -1;
            while(r > 0 && s[r] == ' ')

            {	r-=1;	}

            return s.substring(0, r+1);
        }


        var modify = $('#modify').html();
        var patron = $('#patron').html();
        if ($('#patron') != null){
            var pattern=/P TYPE\:.\d+/gi;
            var broward= /((.*P TYPE\:19)|(.*P TYPE\:2[0-2]))/gi;
            var emailPattern1 = "@";
            var emailPattern2 = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
            var cpatron = patron.replace(/P TYPE:.+/, " ");
            var strInputCode = patron.replace(/&(lt|gt);/g, function (strMatch, p1){
                return (p1 == "lt")? "<" : ">";
            });
            var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "__");

            var arPatron = strTagStrippedText.split("__");

            var chkindex = -1;
            var chkemail = false;
            for(var i = 0; i < arPatron.length; i++)
            {
                if(arPatron[i].match(emailPattern1))
                {
                    chkindex = i;
                    var trimedEmail  = trim(arPatron[i]);
                    escapeEmailString = escape(trimedEmail);
                    var patternSpecialChar = "%0A";
                    var removedSpecialCharEmailString = escapeEmailString.replace(patternSpecialChar, "");
                    if(emailPattern2.test(removedSpecialCharEmailString))
                    {
                        chkemail = true;
                    }

                }
            }
            $('#patron').hide();
            $('#patNameAddress').html(cpatron).addClass('patNameAddress');
            if (patron.match(broward)){
                $('#patNameAddress').html(cpatron + '<br />' + modify);
            }

            if ((patron.match(broward)) && ((chkindex == -1)||(!chkemail))){
                $('#patNameAddress').html(cpatron + '<br />' + modify + emailAllertMsg);
            }
        }
    });
}

function showActionRow(){
    if(requestIndex != -1)
    {
        document.write("<a href" + navrowlinks[requestIndex] +  bibilliadlink + " ");
    }
    if(addcartIndex != -1)
    {
        document.write("<a href" + navrowlinks[addcartIndex]);
    }
    if(removecartIndex != -1)
    {
        document.write("<a href" + navrowlinks[removecartIndex]);
    }
}
//Resource Record
function showResource(){
    var access=document.getElementById("access").innerHTML;
    var accessIcon=/wb_perm_icon/gi;
    var accessNote = access.match(accessIcon);
    if (accessNote == null){
        document.write('<style>#accessResourceLabel,#accessResourceValue{display:none}</style>');
    }
    var subject=document.getElementById("subject").innerHTML;
    var subjectValueClass=/resourceInfoData/gi;
    var subjectValue = subject.match(subjectValueClass);
    if (subjectValue == null){
        document.write('<style>#subjectResourceLabel,#subjectResourceValue{display:none}</style>');
    }
}
//
// JavaScript Document

// set this to be the URL for the SMS script

var smsurl = "http://systems.library.nova.edu/novacat_sms/sms.php?";
function showsms() {

    /*   This function shows the SMS layer and creates the form   */

    try {

        var title = '';		// we'll save the title here
        var debug = 0;		// enable this to show alerts
        var f = document.getElementById('bib_detail');

        try {							// we use try/catch blocks to hide errors
            var tr = document.getElementsByTagName('TR');		// we have to iterate through every TR b/c we can't get to the title otherwise
            for(i = 0; i < tr.length; i++) {					// for every TR in the document
                var xcells=tr[i].getElementsByTagName('TD');			// get all of the Columns
                if (xcells.length == 2 && xcells[0].innerHTML == "Title") {  // if the row has 2 columns and the first one has the text of Title
                    title = xcells[1].innerHTML.replace(/(<([^>]+)>)/ig,""); // strip out all of the HTML so we just have text
                    if (debug > 0) alert('found title: ' + title);		// just a debug notice
                }
            }
        } catch (e) {}

        var sms = document.getElementById('sms');				// this is the DIV that we're going to put the text into
        // we'll load the 'out' variable with all the html and then put it into the sms div
        var out = "<h3>Send the title, location, and call number of this item to your cell.</h3><p><strong>NOTE:</strong> Carrier charges may apply if your cell phone service plan does not include free text messaging.</p>";

        out +="<form name='sms_form' id='sms_form' method='post'>";
        out += '<input type=\"hidden\" name=\"title\" value=\"'+title+'\">';	//dump the title into a hidden form variable
        out += '<fieldset class="phoneinfo"><legend>Phone information: </legend><label>Your cell number:</label><input name="phone" type="text" maxlength="10"> <span class="eg">(Enter the full 10 digits - no spaces, no dashes.)</span>';	// input for the phone #
        out += '<label>Your provider:</label> <select name="provider">';	// pull-down for each of phone carriers - the values will be parsed by the php script
        out += "<option value='cingular'>Cingular/AT&amp;T</option>";
        out += "<option value='cricket'>Cricket</option>";
        out += "<option value='nextel'>Nextel</option>";
        out += "<option value='qwest'>Qwest</option>";
        out += "<option value='sprint'>Sprint</option>";
        out += "<option value='tmobile'>T-Mobile</option>";
        out += "<option value='verizon'>Verizon</option>";
        out += "<option value='virgin'>Virgin</option>";
        out += "</select></fieldset>";
        out += "<fieldset><legend>Item information: </legend><label> "+ title +":</label><ol id='smslist'>";

        var itms = document.getElementById('bib_items');		// get the ITEM table
        var tr = itms.getElementsByTagName('TR');	// get each row
        for(i = 1; i < tr.length && i < 6; i++) {
            var xcells=tr[i].getElementsByTagName('TD');			// get each cell
            if (xcells.length == 3) {								// if there's only 3 cells (like our ITEM table)
                var loc = xcells[0].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");		// get the location (remove tags)
                var callLinks = xcells[1].getElementsByTagName("a"); //get the call number without extras
                var call = callLinks[0].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");
                //var call = xcells[1].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");	// get the call number + copies if any (remove tags)
                var status = xcells[2].innerHTML.replace(/(<([^>]+)>|&nbsp;)/ig,"");	// get the status (remove tags)

                var chck = '';
                if (i == 1) chck = ' checked ';			// if we're on the first row, check it
                // append the input
                out += '<li><input '+chck+' type=\"radio\" name=\"loc\" value=\"'+loc+'|'+call+'\">&nbsp;'+ loc + ": "+call+" ("+status+")</li>";
                // debug statement
                if (debug > 0) alert('found item: ' + loc + '|' + call + ' | ' + status );
            }
        }
        // close the list
        out += "</ol></fieldset>";


        // add buttons at bottom.  note the return false which stops the forms from actually doing anything
        //    out += "<p><a href='#here' id='sendmessage' onClick='sendSMS();return false;'><img src='/screens/smssend.gif' border=0></a> <a href='#here' id='clearmessage' onClick='clearsms();return false;'><img src='/screens/smsclear.gif' border=0></a></p>";

        out += "<p><input type='submit' onClick='sendSMS();return false;' value='Send text message'> <input id='clearmessage' type='reset' onClick='clearsms();return false;' value='Close'></p>";

        // we use the innerHTML property to actually set the HTML into the page
        sms.innerHTML = out+"</form>";

        // now we make the div visible
        sms.style.visibility = 'visible';
        sms.style.display = 'block';
        // some fancy positioning ... or not (bd)
        // findPos(document.getElementById('smsbutton'),sms,25,-320);

        document.sms_form.phone.focus();  // place the cursor in the phone# field

    } catch (e) {
        // doesn't work?  hide the SMS buttons
        document.getElementById('smsfeatures').style.visibility='visible';
        alert('Oops...something about this record doesn\'t agree with the texting mechanism.\nCould be no real call number; could be something else.\nYou\'ll have to write down the call number.');
    }
    return false;
}


function sendSMS(location) {
    var frm = document.sms_form;			// get the SMS form
    var phone = frm.phone.value;			// get the phone #
    phone = phone.replace(/[^\d]/ig,"");	// remove all non-digit characters
    if (phone.length == 10) {				// if 10 chars, we're good
        var url = smsurl;						// start creating the URL
        url += "&number="+encodeURIComponent(frm.phone.value);	// html escape #
        url += "&provider="+encodeURIComponent(frm.provider.options[frm.provider.selectedIndex].value);	// html escpae provider
        for (i=0;i<frm.loc.length;i++) {		// for each item, get the checked one
//		alert(i+" "+frm.loc[i].checked);
            if (frm.loc[i].checked == true) {	// if checked, add it to the URL
                url += "&item="+encodeURIComponent(frm.loc[i].value);
            }
        }
        if (frm.loc.length == undefined) {		// if just one, should not come to this
            url += "&item="+encodeURIComponent(frm.loc.value);
        }

        var bodyRef = document.getElementsByTagName("body")[0]; //get the bib number out of the <body>, add it to the url
        var bodyText = bodyRef.innerHTML;
        var bibNum = bodyText.match(/b[\d]{7}/m);
        url += "&bib="+bibNum;

        var head = document.getElementsByTagName("head")[0];		// now we create a <SCRIPT> tag in the <HEAD> to get the response
        var script = document.createElement('script');
        script.setAttribute('type','text/javascript');
        script.setAttribute('src',url);							// the script is actually the PERL script
        head.appendChild(script);									// append the script
    } else {		// invalid phone #, send message
        alert('Please enter a valid 10-digit phone number');
    }
}

// clear/hide the SMS DIV
function clearsms() {
    var sms = document.getElementById('sms');
    sms.style.visibility = 'hidden';
    sms.style.display = 'none';
}



// get the position of an item, good for putting the SMS form in a useful place
function findPos(obj,obj2,lofset,tofset) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        curleft = obj.offsetLeft
        curtop = obj.offsetTop
        while (obj = obj.offsetParent) {
            curleft += obj.offsetLeft
            curtop += obj.offsetTop
        }
    }
    obj2.style.left = curleft+lofset;
    obj2.style.top = curtop+tofset;
//	return [curleft,curtop];
}

// Grab the bib number of the item
function getbib() {
    var buttonBlock = document.getElementById('navigationRow').innerHTML;
    sms.style.visibility = 'hidden';
    sms.style.display = 'none';
}

//LAFAYETTE FUNCTIONS

// Show/Hide TOC toggles for mobile interface (bibtoc.js)
// By Bob Duncan
function showtoc() {
    document.getElementById("bib_TOC").style.display = "table";
    document.getElementById("showButton").style.display = "none";
    document.getElementById("hideButton").style.display = "inline";
}

function hidetoc() {
    document.getElementById("bib_TOC").style.display = "none";
    document.getElementById("hideButton").style.display = "none";
    document.getElementById("showButton").style.display = "inline";
}

// END TOC toggles

// Write permalink on bib record (permalink2.js)
// By Bob Duncan
function print_permalink()
{
    var PermaLink = document.getElementById("recordnum");
    var tomatch = /record=r/;
    var is_a_match = tomatch.test(PermaLink);
    if (PermaLink != null &! is_a_match){
        var str = PermaLink.toString();
        var PermaLink = str.replace(/~S0/,"");
        document.write ('<div class="permalink">');
        document.write ("Permanent URL for this catalog record:<br />"+PermaLink);
        document.write ("</div>");
    }
    else {}
}

// END permalink

// showItems - created by Andrew Welch, Aurora Public Library.
/* Hides the items table and changes the iii-default 'There are additional copies...'
 table--which appears on briefcit when a bib has more than 3 attached items--to
 custom text (specified in msg) that links to the full record (or holdings table). */

function showItems(msg) {
    var itemsDiv = getElementsByClassName(document, "div", "briefCiteItems");
    var bibTitle = getElementsByClassName(document, "div", "briefCiteTitle");
    var bibLink;
    if (itemsDiv.length == 0) {} // There are no item tables, or this is not a briefcit page;
    else {
        for (var i=0; i<itemsDiv.length; i++) {
            var getTabs = itemsDiv[i].getElementsByTagName('table'); // Find all tables in the itemsDiv;
            if (getTabs.length <= 1 || !document.createElement) {} // There is no 'additional copies' table, so do nothing;
            else {
                var addlTab = getTabs[1].rows[0].cells[0]; // isolate the system-generated message...
                getTabs[1].rows[0].cells[0].innerHTML = ""; // ...and delete it.
                getTabs[0].style.display = "none"; // Hides the items table - comment out this line if you want to show the items table;
                var newA = document.createElement('a'); // Create an anchor tag;
                var newMsg = document.createTextNode('See full record for location, call number, and status.'); // Create the new message;
                var bibT = bibTitle[i].getElementsByTagName('a');
                if (bibT.length == 0) {}
                bibLink = bibT[0].href; // Option 1: Creates a link to the full record from the bib's Title link;
//				bibLink = bibT[0].href.replace(/frameset/,"holdings"); // Option 2: Creates a link to the full holdings table instead of the full record;
                addlTab.appendChild(newA); // Put the new anchor tag in the 'additional copies' table;
                newA.appendChild(newMsg); // Put the new message inside the anchor tag;
                newA.setAttribute('href',bibLink); // Set the anchor tag's href to be the bibLink value;
                newA.className = "moreItems"; // Use this to give the new link a className (put it between the quotes);
            }
        }
    }
}

// --------------------------------------------------------------------
// getElementsByClassName()
// Written by Jonathan Snook, http://www.snook.ca/jonathan
// Add-ons by Robert Nyman, http://www.robertnyman.com
// Can be called multiple ways:
//  Get all 'a' elements with the 'info-links' class within a document:
//    getElementsByClassName(document, "a", "info-links");
//  Get all 'div' elements with a 'col' class within an element with a 'container' id:
//    getElementsByClassName(document.getElementById("container"), "div", "col");
//  Get all elements with a 'click-me' class within a document:
//    getElementsByClassName(document, "*", "click-me");
// --------------------------------------------------------------------
function getElementsByClassName(oElm, strTagName, strClassName){
    var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
    var arrReturnElements = new Array();
    strClassName = strClassName.replace(/-/g, "\-");
    var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
    var oElement;
    for(var i=0; i<arrElements.length; i++){
        oElement = arrElements[i];
        if(oRegExp.test(oElement.className)){
            arrReturnElements.push(oElement);
        }
    }
    return (arrReturnElements)
} // End of getElementsByClassName()

function getHoldings(pageURL, obj) {
    var ajaxOK = createAjaxObj();
    if (!ajaxOK) {}
    else if (ajaxOK) {
//		var obj = getElementsByClassName(document,'div',divClass);
        ajaxOK.open("GET", pageURL);

        ajaxOK.onreadystatechange = function() {
            if (ajaxOK.readyState == 4 &&
                ajaxOK.status == 200) {
                innerdiv = ajaxOK.responseText;
                var tabStart = innerdiv.indexOf('class="additionalCopies"');
                var tabEnd = innerdiv.lastIndexOf('class="additionalCopiesNav"');
                obj.innerHTML = innerdiv.slice(tabStart+25,tabEnd);
//				alert(obj.innerHTML);
            }
        }
        ajaxOK.send(null);
    }
}

// END showitems and related scripts

// Write link to add bib info to RefWorks(refworks.js)
// Called by WebBridge resource #262
function print_refworks()
{
    var PermaLink = document.getElementById("recordnum");
    var str = PermaLink.toString();
    var RWLink = str.replace(/libcat\.lafayette\.edu\/record=/,"libkit.lafayette.edu/webbridge/processor_a.php?recordnum=");
    if (PermaLink != null) {
        document.write ("Export information for this item to <a href='"+RWLink+"' target='_blank'>RefWorks</a>.");
    }
}

// END print_refworks

// Send requests associated with order records to request forms on library.laf (orderRecMsg.js)
// Called by messages.conf file to use with WebBridge resources 399-401
function get_values() {
    PermaLink = document.getElementById("recordnum");
    str = PermaLink.toString();
    recnum = str.replace(/.+record=(.+)~S.+/,"$1a");
    if (document.getElementById("wbReqLink") != null){
        requestLink = document.getElementById("wbReqLink").innerHTML;
    }
    else {
        requestLink = null;
    }
}

function print_inProcess()
{
    if (document.getElementById("wbReqLink") != null){
        get_values();
        document.write ("&#171; <a href='"+requestLink+recnum+"' target='_blank'>Request this item</a> &#187;");
    }
    else {
        document.write ("Please contact a reference librarian to request this item.");
    }
}

function print_onOrder()
{
    if (document.getElementById("wbReqLink") != null){
        get_values();
        document.write ("&#171; <a href='"+requestLink+recnum+"' target='_blank'>Request notification when this item becomes available</a> &#187;");
    }
    else {
        document.write ("Please contact a reference librarian to request notification when this item becomes available.");
    }

}

// END order record messages

// If cover from Muse exists, don't display one from JSTOR (scoverimg.js)
function kill_JstorImg () {

    var jstorimg = document.jstor_cover_image448_form;
    var museimg = document.project_muse_cover_image449_form;
    if (jstorimg && museimg) {
        jstorimg.style.display = "none";
    }
}

// END kill JSTOR img

// Print "Record x of y" on bib record displays following KW search (printRecNums.js)
// Extracts hit number from window URL
// By Bob Duncan
function printRecNums() {
    var bodyRef = document.getElementsByTagName("body")[0];
    var bodyText = bodyRef.innerHTML;
    var results = bodyText.match(/([\d]+) results found/m);
    if (results) {
        var resultsNum = results[1];
        var winloc = location.href;
        var isHex = winloc.match(/&.+%2C([\d]+)%2C.*$/m);
        var isCommas = winloc.match(/([\d]+),\?save.+$/m);
        if (isHex){
            recNum = isHex[1];
        }
        else if (isCommas) {
            recNum = isCommas[1];
        }
        var navMessage = "Record " + recNum + " of " + resultsNum;
        document.write (navMessage);
    }
}
// END print record numbers

// Offer the ability to turn off/on keyword highlighting (hilite.js)
// By Bob Duncan
function writeHilites () {
    var hilitesDiv = document.getElementById("hilitesSwitch");
    var hilites = document.body.getElementsByTagName("font");
//var hilitesLink = '<a href="#" onclick="removehilite();"><img src="/screens/1_hiliter-off.gif" alt="Hilite Off" /></a>';
    var hilitesLink = '<a href="#" onclick="removehilite();">Highlight Off</a>';
    var winloc = location.href;
    var isKWSearch = winloc.match(/FF=X/);
    if (isKWSearch && hilites.length > 0) {
        hilitesDiv.innerHTML = hilitesLink;
    }
}

function removehilite() {
    var hilites = document.body.getElementsByTagName("font");
//var onlink = '<a href="#" onclick="restorehilite();"><img src="/screens/1_hiliter-on.gif" alt="Hilite On" width="47" height="16" /></a>'
    var onlink = '<a href="#" onclick="restorehilite();">Highlight On</a>'
    for(var i =0; i < hilites.length; i++) {
        hilites[i].setAttribute("class", "kwhilite");
        hilites[i].setAttribute("className", "kwhilite"); // for IE
        hilites[i].removeAttribute("color");
    }
    document.getElementById("hilitesSwitch").innerHTML = onlink;
}

function restorehilite() {
    var hilites = document.body.getElementsByTagName("font");
//var offlink = '<a href="#" onclick="removehilite();"><img src="/screens/1_hiliter-off.gif" alt="Hilite Off" width="47" height="16" /></a>';
    var offlink = '<a href="#" onclick="removehilite();">Highlight Off</a>';

    for(var i =0; i < hilites.length; i++) {
        hilites[i].removeAttribute("class");
        hilites[i].removeAttribute("className");  // for IE
    }
    document.getElementById("hilitesSwitch").innerHTML = offlink;
}
// END keyword highlighting

// Change space dash dash space into HTML line break in course reserves records (makeitbetter.js)
// CSS makes it even betterer
// Inspiration by Brent Searle, Langara College. Perspiration by Bob Duncan, Lafayette College

function makeitbetter() {
    var td = document.getElementsByTagName('TD');
    for (i = 1; i<td.length; i++) {
        var textcell = td[i].innerHTML.match(/ -- /);
        if (textcell) {
            var better = td[i].innerHTML.replace(/ -- /g, "<br />");
            td[i].innerHTML = better;
        }
        if (document.forms[1]){
            document.forms[1].elements["sort_title"].value = 'Sort by Title';
            document.forms[1].elements["sort_author"].value = 'Sort by Author';
            document.forms[1].elements["sort_call"].value = 'Sort by Call #';
        }
    }
}

// END makeitbetter

//GOOGLE BOOKS
/*
 *   GBS - Google Book Classes
 *
 *   Copyright 2008 by Godmar Back godmar@gmail.com
 *
 *   License: This software is released under the LGPL license,
 *   See http://www.gnu.org/licenses/lgpl.txt
 *
 *   $Id: gbsclasses.js,v 1.6 2008/04/27 03:17:14 gback Exp gback $
 *
 *   Instructions:
 *   ------------
 *
 *  Add <script type="text/javascript" src="gbsclasses.js"></script>
 *  to your document.
 */

(function() {

    gbs = {
        isReady: false,
        readyListeners: new Array()
    };

    /*****************************************************************/
    /*
     * Add an event handler, browser-compatible.
     * This code taken from http://www.dustindiaz.com/rock-solid-addevent/
     * See also http://www.quirksmode.org/js/events_compinfo.html
     *          http://novemberborn.net/javascript/event-cache
     */
    function addEvent( obj, type, fn ) {
        if (obj.addEventListener) {
            obj.addEventListener( type, fn, false );
            EventCache.add(obj, type, fn);
        }
        else if (obj.attachEvent) {
            obj["e"+type+fn] = fn;
            obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
            obj.attachEvent( "on"+type, obj[type+fn] );
            EventCache.add(obj, type, fn);
        }
        else {
            obj["on"+type] = obj["e"+type+fn];
        }
    }

    /* unload all event handlers on page unload to avoid memory leaks */
    var EventCache = function(){
        var listEvents = [];
        return {
            listEvents : listEvents,
            add : function(node, sEventName, fHandler){
                listEvents.push(arguments);
            },
            flush : function(){
                var i, item;
                for(i = listEvents.length - 1; i >= 0; i = i - 1){
                    item = listEvents[i];
                    if(item[0].removeEventListener){
                        item[0].removeEventListener(item[1], item[2], item[3]);
                    };
                    if(item[1].substring(0, 2) != "on"){
                        item[1] = "on" + item[1];
                    };
                    if(item[0].detachEvent){
                        item[0].detachEvent(item[1], item[2]);
                    };
                    item[0][item[1]] = null;
                };
            }
        };
    }();
    addEvent(window,'unload',EventCache.flush);
// end of rock-solid addEvent

    /**
     * Browser sniffing and document.ready code taken from jQuery.
     *
     * Source: jQuery (jquery.com)
     * Copyright (c) 2008 John Resig (jquery.com)
     */
    var userAgent = navigator.userAgent.toLowerCase();

// Figure out what browser is being used
    gbs.browser = {
        version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [])[1],
        safari: /webkit/.test( userAgent ),
        opera: /opera/.test( userAgent ),
        msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
        mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
    };

    function bindReady() {
        // Mozilla, Opera (see further below for it) and webkit nightlies currently support this event
        if ( document.addEventListener && !gbs.browser.opera)
        // Use the handy event callback
            document.addEventListener( "DOMContentLoaded", gbs.ready, false );

        // If IE is used and is not in a frame
        // Continually check to see if the document is ready
        if ( gbs.browser.msie && window == top ) (function(){
            if (gbs.isReady) return;
            try {
                // If IE is used, use the trick by Diego Perini
                // http://javascript.nwbox.com/IEContentLoaded/
                document.documentElement.doScroll("left");
            } catch( error ) {
                setTimeout( arguments.callee, 0 );
                return;
            }
            // and execute any waiting functions
            gbs.ready();
        })();

        if ( gbs.browser.opera )
            document.addEventListener( "DOMContentLoaded", function () {
                if (gbs.isReady) return;
                for (var i = 0; i < document.styleSheets.length; i++)
                    if (document.styleSheets[i].disabled) {
                        setTimeout( arguments.callee, 0 );
                        return;
                    }
                // and execute any waiting functions
                gbs.ready();
            }, false);

        if ( gbs.browser.safari ) {
            //var numStyles;
            (function(){
                if (gbs.isReady) return;
                if ( document.readyState != "loaded" && document.readyState != "complete" ) {
                    setTimeout( arguments.callee, 0 );
                    return;
                }
                /*
                 if ( numStyles === undefined )
                 numStyles = jQuery("style, link[rel=stylesheet]").length;
                 if ( document.styleSheets.length != numStyles ) {
                 setTimeout( arguments.callee, 0 );
                 return;
                 }
                 */
                // and execute any waiting functions
                gbs.ready();
            })();
        }

        // A fallback to window.onload, that will always work
        addEvent(window, "load", gbs.ready);
    }

// end of code taken from jQuery

    gbs.ready = function () {
        if (gbs.isReady)
            return;
        gbs.isReady = true;

        for (var i = 0; i < gbs.readyListeners.length; i++) {
            gbs.readyListeners[i]();
        }
    }

    function trim(s) {
        return s.replace(/^\s+|\s+$/g, '');
    };

// Begin GBS code
    var gbsKey2Req;         // map gbsKey -> Array<Request>
    gbs.ProcessResults = function (bookInfo) {
        for (var i in bookInfo) {
            var result = bookInfo[i];
            // alert("result found for: " + result.bib_key + " " + result.preview);
            req = gbsKey2Req[result.bib_key];
            if (req == null) {
                continue;
            }

            for (var j = 0; j < req.length; j++) {
                req[j].onsuccess(result);
            }
            delete gbsKey2Req[result.bib_key];
        }

        for (var i in gbsKey2Req) {
            req = gbsKey2Req[i];
            if (req == null)
                continue;

            for (var j = 0; j < req.length; j++) {
                req[j].onfailure();
            }
            delete gbsKey2Req[i];
        }
    }

// process all spans in a document
    function gbsProcessSpans(spanElems) {
        gbsKey2Req = new Object();
        while (spanElems.length > 0) {
            var spanElem = spanElems.pop();
            if (spanElem.expanded)
                continue;

            gbsProcessSpan(spanElem);
        }

        var bibkeys = new Array();
        for (var k in gbsKey2Req)
            bibkeys.push(k);

        if (bibkeys.length == 0)
            return;

        bibkeys = bibkeys.join(",");
        var s = document.createElement("script");
        s.setAttribute("type", "text/javascript");
        // alert("searching for: " + bibkeys);
        s.setAttribute("src", "https://books.google.com/books?jscmd=viewapi&bibkeys=" + bibkeys + "&callback=gbs.ProcessResults");
        document.documentElement.firstChild.appendChild(s);
    }

// process a single span
    function gbsProcessSpan(spanElem) {
        var cName = spanElem.className;
        if (cName == null)
            return;

        var mReq = {
            span: spanElem,
            removeTitle: function () {
                this.span.setAttribute('title', '');
            },
            success: new Array(),
            failure: new Array(),
            onsuccess: function (result) {
                for (var i = 0; i < this.success.length; i++)
                    try {
                        this.success[i](this, result);
                    } catch (er) { }
                this.removeTitle();
            },
            onfailure: function (status) {
                for (var i = 0; i < this.failure.length; i++)
                    try {
                        this.failure[i](this, status);
                    } catch (er) { }
                this.removeTitle();
            },
            /* get the search item that's sent to GBS
             * The search term may be in the title, or in the body.
             * It's in the body if the title contains a "*".
             * Example:
             *           <span title="ISBN:0743226720"></span>
             *           <span title="*">ISBN:0743226720</span>
             */
            getSearchItem: function () {
                if (this.searchitem === undefined) {
                    var req = this.span.getAttribute('title');
                    if (req == "*" || req == "OCLC:*" || req == "LCCN:*") {
                        var text = this.span.innerText || this.span.textContent || "";
                        text = trim(text);
                        this.searchitem = text;     // default

                        switch (req) {
                            case "*":
                                var m = text.match(/^((\d|x){10,13}).*/i);
                                if (m) {
                                    this.searchitem = "ISBN:" + m[1];
                                }
                                break;

                            case  "OCLC:*":
                                text = text.replace(/^ocm/, "");
                                this.searchitem = "OCLC:" + text;
                                break;

                            case  "LCCN:*":
                                this.searchitem = "LCCN:" + text;
                                break;
                        }

                        // remove first child only
                        if (this.span.hasChildNodes())
                            this.span.removeChild(this.span.firstChild);
                    } else
                        this.searchitem = req.toLowerCase();
                }
                return this.searchitem;
            }
        };

        // wrap the span element in a link to bookinfo[bookInfoProp]
        function linkTo(mReq, bookInfoProp) {
            mReq.success.push(function (mReq, bookinfo) {
                if (bookinfo[bookInfoProp] === undefined)
                    return;

                var p = mReq.span.parentNode;
                var s = mReq.span.nextSibling;
                p.removeChild(mReq.span);
                var a = document.createElement("a");
                a.setAttribute("href", bookinfo[bookInfoProp]);
                a.appendChild(mReq.span);
                p.insertBefore(a, s);
            });
        }

        /**
         * See: http://code.google.com/apis/books/getting-started.html
         bib_key
         The identifier used to query this book.
         info_url
         A URL to a page within Google Book Search with information on the book (the about this book page)
         preview_url
         A URL to the preview of the book - this lands the user on the cover of the book.
         If there only Snippet View or Metadata View books available for a request, no preview url
         will be returned.
         thumbnail_url
         A URL to a thumbnail of the cover of the book.
         preview
         Viewability state - either "noview", "partial", or "full"
         */
        function addHandler(gbsClass, mReq) {
            switch (gbsClass) {
                case "gbs-thumbnail":
                case "gbs-thumbnail-large":
                    mReq.success.push(function (mReq, bookinfo) {
                        if (bookinfo.thumbnail_url) {
                            var img = document.createElement("img");
                            var imgurl = bookinfo.thumbnail_url;
                            if (gbsClass == "gbs-thumbnail-large") {
                                imgurl = imgurl.replace(/&zoom=5&/, "&zoom=1&")
                                    .replace(/&pg=PP1&/, "&printsec=frontcover&");
                            }
                            img.setAttribute("src", imgurl);
                            mReq.span.appendChild(img);
                        }
                    });
                    break;

                case "gbs-link-to-preview":
                    linkTo(mReq, 'preview_url');
                    break;

                case "gbs-link-to-info":
                    linkTo(mReq, 'info_url');
                    break;

                case "gbs-link-to-thumbnail":
                    linkTo(mReq, 'thumbnail_url');
                    break;

                case "gbs-if-noview":
                case "gbs-if-partial-or-full":
                case "gbs-if-partial":
                case "gbs-if-full":
                    mReq.gbsif = gbsClass.replace(/gbs-if-/, "");
                    mReq.success.push(function (mReq, bookinfo) {
                        if (mReq.gbsif == "partial-or-full") {
                            var keep = bookinfo.preview == "partial" || bookinfo.preview == "full";
                        } else {
                            var keep = bookinfo.preview == mReq.gbsif;
                        }
                        // remove if availability doesn't match the requested
                        if (!keep) {
                            mReq.span.parentNode.removeChild(mReq.span);
                        } else {
                            // else, if span was previously hidden, set visibility to "inline"
                            if (mReq.span.style.display == "none") {
                                mReq.span.style.display = "inline";
                            }
                        }
                    });
                    break;

                case "gbs-remove-on-failure":
                    mReq.failure.push(function (mReq, status) {
                        mReq.span.parentNode.removeChild(mReq.span);
                    });
                    break;
                // XXX add more here

                default:
                    return false;
            }
            return true;
        }

        var isGBS = false;
        var classEntries = cName.split(/\s+/);
        for (var i = 0; i < classEntries.length; i++) {
            if (addHandler(classEntries[i], mReq))
                isGBS = true;
        }

        if (!isGBS)
            return;

        var bibkey = mReq.getSearchItem();
        if (gbsKey2Req[bibkey] === undefined) {
            gbsKey2Req[bibkey] = [ mReq ];
        } else {
            gbsKey2Req[bibkey].push(mReq);
        }
        mReq.span.expanded = true;
    }

    function gbsReady() {
        var span = document.getElementsByTagName("span");
        var spanElems = new Array();
        for (var i = 0; i < span.length; i++) {
            spanElems[i] = span[span.length - 1 - i];
        }
        gbsProcessSpans(spanElems);
    }

    gbs.readyListeners.push(gbsReady);

    bindReady();

})();
