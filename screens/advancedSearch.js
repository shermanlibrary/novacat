/* ==================
 * Upfront Scripts
 * ================== */
/* ==================
 * Cookies */
(function($,document,undefined){var pluses=/\+/g;function raw(s){return s}function decoded(s){return decodeURIComponent(s.replace(pluses," "))}var config=$.cookie=function(key,value,options){if(value!==undefined){options=$.extend({},config.defaults,options);if(value===null)options.expires=-1;if(typeof options.expires==="number"){var days=options.expires,t=options.expires=new Date;t.setDate(t.getDate()+days)}value=config.json?JSON.stringify(value):String(value);return document.cookie=[encodeURIComponent(key), "=",config.raw?value:encodeURIComponent(value),options.expires?"; expires="+options.expires.toUTCString():"",options.path?"; path="+options.path:"",options.domain?"; domain="+options.domain:"",options.secure?"; secure":""].join("")}var decode=config.raw?raw:decoded;var cookies=document.cookie.split("; ");for(var i=0,l=cookies.length;i<l;i++){var parts=cookies[i].split("=");if(decode(parts.shift())===key){var cookie=decode(parts.join("="));return config.json?JSON.parse(cookie):cookie}}return null}; config.defaults={};$.removeCookie=function(key,options){if($.cookie(key)!==null){$.cookie(key,null,options);return true}return false}})(jQuery,document);
$.cookie('novacat_avs', '', { expire: 1, path: '/'});

(function() {
/* ==================
 * Variables
 * ================== */
	 
	 /* ==================
	  * Reset the Form
	  * ================== */

	  reset_search_fields = function() {
	  	var hidden_search_field 	= $('a.third, a.fourth'),
	  		visible_search_field 	= $('a.second');

	  		hidden_search_fields.parent().css('display','none');
	  		visible_search_field.parent().css('display','block');
	  }

	/* ==================
	 * Additional Keywords
	 * ================== */
	$('a.addTerm').on('click', function( e ) {

		if ( $(this).hasClass('disabled') ) {

			// First, we don't want users to delete the
			// original field, but we definitely want them to 
			// have the option to delete an additional keyword
			// on second thought, but we want to ensure that
			// there are always two fields remaining. We'll
			// just make sure that when the patron deletes
			// the next-to-last term, it strips its value 
			// and placeholder (to prevent confusion), and
			// remove the button class 'disabled'.

			if ( ($(this).hasClass('first')) ) {}

			else if ( $(this).hasClass('second') && !($('.third, .fourth').is(':visible')) ) {
				$('input[name=searchText2]').attr({
					value: '',
					placeholder: ''
				});
				reset_search_fields();
			}

			else if ( $(this).hasClass('third') && !($('.second, .fourth').is(':visible')) ) {
				$('input[name=searchText3').attr({
					value: '',
					placeholder: ''
				});
				reset_search_fields();
			}

			else if ( $(this).hasClass('fourth') && !($('.second, .third').is(':visible')) ) {
				$('input[name=searchText4').attr({
					value: '',
					placeholder: ''
				});
				reset_search_fields();
			}

			else {
				$(this).removeClass('disabled').parent().hide();
				$(this).parent().find('input[type=text]').attr({
					value: '',
					placeholder: ''
				});

			}

		} else if ( $(this).hasClass('fourth') && ($('.third').is(':visible')) ) {
			$('.second').addClass('disabled').parent().show();
		} 


		else {
			$(this).addClass('disabled').parent().next().show();
		}

		e.preventDefault();
	});


	/* ==================
	 * Sort Results
	 * ================== */
	$('a.sort').on('click', function( e ) {

		var sort_value = $(this).attr('value');

		$('.sort').removeClass('selected');
		$(this).addClass('selected');
		$('#SORT').attr('value',sort_value);

		e.preventDefault();
	});

/* ==================
 * Type Limits
 * ================== */
	var $allLabels = $('.accordion dt'),
		$allPanels = $('.accordion > dd, .accordion > ul').hide();

	$('dt.allMaterials').addClass('selected');

	display_infographic = function() { 
	
		$('.explain-check-everything').css('backgroundPosition','0 -132px').show(); 
	
	}
	hide_infographic = function() { $('.explain-check-everything').hide(); }

	initLimits = function () {

		/* Let's Play the Accordion ---------------*/

			$('.accordion > dt > a').on('click', function() {

				var $this = $(this),
					$target = $this.parent().next(),
					$label = $this.parent();

				if ( !$target.hasClass( 'active' ) ) {

					$allPanels.removeClass( 'active' ).hide();
					$allLabels.removeClass( 'selected' );
					$target.addClass( 'active' ).show();
					$label.addClass( 'selected' );

					hide_infographic();
				}

				return false;
			});

			tangoWithCheckboxes();

	}	

	tangoWithCheckboxes = function() {

	var $allMaterials = $('#multimedia,#online,#printed,#misc,.multi,.onres,.print,.misc'),
		$misc = $('#misc'),
		$miscMaterials = $('.misc'),
		$multiMaterials = $('.multi'),
		$multimedia = $('#multimedia'),
		$online = $('#online'),
		$onlineMaterials = $('.onres'), 
		$printed = $('#printed'),
		$printedMaterials = $('.print'),
		$selectAll = $('#selectall'),
		$type = $('.type');

		$selectAll.on('click', function() {
			$allMaterials.attr('checked', this.checked);
			$allPanels.removeClass('active').hide();
			$allLabels.removeClass('selected');
			display_infographic();

		});



		// When a material category is selected, all belonging items are selected
		$multimedia.on('click', function () { $multiMaterials.attr('checked', this.checked); });
		$online.on('click', function () { $onlineMaterials.attr('checked', this.checked); });
		$printed.on('click', function () { $printedMaterials.attr('checked', this.checked); });
		$misc.on('click', function () { $miscMaterials.attr('checked', this.checked); });
	
	
		// When all the itmes in a category are checked,
			//select entire category and collapse menu
		$multiMaterials.on('click', function() {
			if ( $multiMaterials.length == $(".multi:checked").length ) {

				$multimedia.attr("checked", "checked");

			} else {

				$multimedia.removeAttr("checked");
				$selectAll.removeAttr("checked");
			}

		});
		
		$onlineMaterials.on('click', function(){
			
			if( $onlineMaterials.length == $(".onres:checked").length) {
				$online.attr("checked", "checked");
			} else {
				$online.removeAttr("checked");
				$selectAll.removeAttr("checked");
			}

		});
		
		$printedMaterials.on('click', function(){

			if($printedMaterials.length == $(".print:checked").length) {
				$printed.attr("checked", "checked");
			} else {
				$printed.removeAttr("checked");
				$selectAll.removeAttr("checked");
			}

		});
		
		$miscMaterials.click(function(){
			if($miscMaterials.length == $(".misc:checked").length) {
				$misc.attr("checked", "checked");
			} else {
				$misc.removeAttr("checked");
				$selectAll.removeAttr("checked");
			}

		});
		
		$($allMaterials, $type).on('click', function() {

			if($type.length == $(".type:checked").length) {
				$selectAll.attr("checked", "checked");			
				$allPanels.removeClass('active').hide(); 
				$allLabels.removeClass('selected');
				$('dt#searchAll').addClass('selected');
				display_infographic();
			} else {
				
				$selectAll.removeAttr("checked");
			}

		});

	}

   queryString = function() {

	var chosen_materials	= $('input[name=m]:checked').serializeArray(),
		material_limit		= $('#results');
	     
		$.each(chosen_materials, function( i, chosen_material ) {

			material_limit.append('&amp;m=' + chosen_material.value);

		});
	}

	
    $(':submit').on('click', function ( e ) { 
    	queryString();
     });
    initLimits();
})();

