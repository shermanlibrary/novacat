function format_bm() { 
		var bm  = $('.browseSearchtoolMessage'),
		bm_text = bm.text(),
		bm_html	= bm.html(),
		types 		= [];		
		
		bm.hide();		
		
		var pattern		= /\d*\sresults found\s*/,
		    results		= pattern.exec(bm_text);
					
		var pattern		= /Sorted by.*/,
		    sort_html	= pattern.exec(bm_html);
					
			
		var a =/PRINT MATERIAL/,
			a = a.test(bm_text);
			if(a){types.push('"Print Material"');}	
			
		var b =/E-JOURNAL/,
			b = b.test(bm_text);
			if(b){types.push('"E-Journal"');}	
			
		var c =/SCORE/,
			c = c.test(bm_text);
			if(c){types.push('"Score"');}	
			
		var d =/DISSERTATION/,
			d = d.test(bm_text);
			if(d){types.push('"Dissertation"');}	
			
		var e =/MAP\/ATLAS/,
			e = e.test(bm_text);
			if(e){types.push('"Map/Atlas"');}	
			
		var f =/STREAMING MEDIA/,
			f = f.test(bm_text);
			if(f){types.push('"Streaming Video / Audio"');}	
			
		var g =/DVD/,
			g = g.test(bm_text);
			if(g){types.push('"DVD"');}	
			
		var h =/WEB/,
			h = h.test(bm_text);
			if(h){types.push('"Web"');}	
			
		var i =/NON-MUSIC CD/,
			i = i.test(bm_text);
			if(i){types.push('"Audiobook Spoken Word"');}	
			
		var j =/MUSIC CD/,
			j = j.test(bm_text);
			if(j){types.push('"Music CD"');}	
			
		var k =/2-D GRAPHIC/,
			k = k.test(bm_text);
			if(k){types.push('"Slides"');}	
			
		var l =/HARDWARE/,
			l = l.test(bm_text);
			if(l){types.push('"Equipment"');}	
			
		var m =/SOFTWARE/,
			m = m.test(bm_text);
			if(m){types.push('"Software"');}	
		
		var n =/E-BOOK/,
			n = n.test(bm_text);
			if(n){types.push('"E-Book"');}	
			
		var o =/KIT/,
			o = o.test(bm_text);
			if(o){types.push('"Kit"');}	
			
		var p =/LARGE PRINT/,
			p = p.test(bm_text);
			if(p){types.push('"Large Print"');}	
			
		var q =/PLAYAWAY/,
			q = q.test(bm_text);
			if(q){types.push('"Playway"');}	
			
		var r =/3-D OBJECT/,
			r = r.test(bm_text);
			if(r){types.push('"LeapPad / Toys"');}	
			
		var s =/STUDY ROOM/,
			s = s.test(bm_text);
			if(s){types.push('"Study Room"');}	
			
		var t =/ARCHIVAL MATL/,
			t = t.test(bm_text);
			if(t){types.push('"Archival Material"');}	
			
		var x =/MICROFORM/,
			x = x.test(bm_text);
			if(x){types.push('"Microform"');}	
			
		var _1 =/BLU-RAY/,
			_1 = _1.test(bm_text);
			if(_1){types.push('"Blu-ray"');}	
			
		var _2 =/VHS/,
			_2 = _2.test(bm_text);
			if(_2){types.push('"VHS"');}
			
		var _3 =/VIDEO GAME/,
			_3 = _3.test(bm_text);
			if(_3){types.push('"Video Game"');}
			
		var _5 =/CASSETTE/,
			_5 = _5.test(bm_text);
			if(_5){types.push('"Cassette"');}
			
		var _6 =/MP3 CD/,
			_6 = _6.test(bm_text);
			if(_6){types.push('"MP3 CD"');}	
			
		var _7 =/AUDIO DOWNLOAD/,
			_7 = _7.test(bm_text);
			if(_7){types.push('"Audio Download"');}
		
		sort = (sort_html != '') ? sort_html: ''; 
		
		var mats 	= types.sort().join(" or "),
			limited = (types.length >= 1) ? '<i>Limited to:</i> Material Type ' : '';
			bm.html(limited+ mats +' <i>'+ results +'.</i> '+ sort +'</div>').show();
	}

format_bm();
