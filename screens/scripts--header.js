/* ==================
 * Contents
 * ================== */
/* ==================
 * 1. Upfront Scripts
   -. Responsive IE8 Polyfill
   2. Upfront Variables
   3. Conditional Responsive Script
   4. Functions
   -. Bread Crumbs
   -. Menu
   --. Waypoints
   --. Mobile Menu Toggle
   --. Multi-Toggle Dropdown
   --. Sub-Menu Toggle
   --. Persistent Menu
   --. Portlet Sub-Menu
   -. Special Templating
 */

/* ==================
 * Responsive Script */
// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

/* ==================
 * Upfront Variables
 * ================== */
var responsive_viewport = $(window).width();


/* ==================
 * FUNCTIONS
 * ================== */

/* ==================
 * Menu */
tinsleyfied_menu = function() {
	var menu 					= $('nav.menu li'),
		menuItem				= $('nav.menu li > a.link'),
		menuLink				= $('nav.menu a.menu-link'),
		subMenuParent			= $('li.has-subnav a'),
		subMenu 				= $('ul.children'),
		menuUL					= $('nav.menu > ul');

	/* ==================
	 * Waypoints
	 * ================== */
	(function($,k,m,i,d){var e=$(i),g="waypoint.reached",b=function(o,n){o.element.trigger(g,n);if(o.options.triggerOnce){o.element[k]("destroy")}},h=function(p,o){if(!o){return -1}var n=o.waypoints.length-1;while(n>=0&&o.waypoints[n].element[0]!==p[0]){n-=1}return n},f=[],l=function(n){$.extend(this,{element:$(n),oldScroll:0,waypoints:[],didScroll:false,didResize:false,doScroll:$.proxy(function(){var q=this.element.scrollTop(),p=q>this.oldScroll,s=this,r=$.grep(this.waypoints,function(u,t){return p?(u.offset>s.oldScroll&&u.offset<=q):(u.offset<=s.oldScroll&&u.offset>q)}),o=r.length;if(!this.oldScroll||!q){$[m]("refresh")}this.oldScroll=q;if(!o){return}if(!p){r.reverse()}$.each(r,function(u,t){if(t.options.continuous||u===o-1){b(t,[p?"down":"up"])}})},this)});$(n).bind("scroll.waypoints",$.proxy(function(){if(!this.didScroll){this.didScroll=true;i.setTimeout($.proxy(function(){this.doScroll();this.didScroll=false},this),$[m].settings.scrollThrottle)}},this)).bind("resize.waypoints",$.proxy(function(){if(!this.didResize){this.didResize=true;i.setTimeout($.proxy(function(){$[m]("refresh");this.didResize=false},this),$[m].settings.resizeThrottle)}},this));e.load($.proxy(function(){this.doScroll()},this))},j=function(n){var o=null;$.each(f,function(p,q){if(q.element[0]===n){o=q;return false}});return o},c={init:function(o,n){this.each(function(){var u=$.fn[k].defaults.context,q,t=$(this);if(n&&n.context){u=n.context}if(!$.isWindow(u)){u=t.closest(u)[0]}q=j(u);if(!q){q=new l(u);f.push(q)}var p=h(t,q),s=p<0?$.fn[k].defaults:q.waypoints[p].options,r=$.extend({},s,n);r.offset=r.offset==="bottom-in-view"?function(){var v=$.isWindow(u)?$[m]("viewportHeight"):$(u).height();return v-$(this).outerHeight()}:r.offset;if(p<0){q.waypoints.push({element:t,offset:null,options:r})}else{q.waypoints[p].options=r}if(o){t.bind(g,o)}if(n&&n.handler){t.bind(g,n.handler)}});$[m]("refresh");return this},remove:function(){return this.each(function(o,p){var n=$(p);$.each(f,function(r,s){var q=h(n,s);if(q>=0){s.waypoints.splice(q,1);if(!s.waypoints.length){s.element.unbind("scroll.waypoints resize.waypoints");f.splice(r,1)}}})})},destroy:function(){return this.unbind(g)[k]("remove")}},a={refresh:function(){$.each(f,function(r,s){var q=$.isWindow(s.element[0]),n=q?0:s.element.offset().top,p=q?$[m]("viewportHeight"):s.element.height(),o=q?0:s.element.scrollTop();$.each(s.waypoints,function(u,x){if(!x){return}var t=x.options.offset,w=x.offset;if(typeof x.options.offset==="function"){t=x.options.offset.apply(x.element)}else{if(typeof x.options.offset==="string"){var v=parseFloat(x.options.offset);t=x.options.offset.indexOf("%")?Math.ceil(p*(v/100)):v}}x.offset=x.element.offset().top-n+o-t;if(x.options.onlyOnScroll){return}if(w!==null&&s.oldScroll>w&&s.oldScroll<=x.offset){b(x,["up"])}else{if(w!==null&&s.oldScroll<w&&s.oldScroll>=x.offset){b(x,["down"])}else{if(!w&&s.element.scrollTop()>x.offset){b(x,["down"])}}}});s.waypoints.sort(function(u,t){return u.offset-t.offset})})},viewportHeight:function(){return(i.innerHeight?i.innerHeight:e.height())},aggregate:function(){var n=$();$.each(f,function(o,p){$.each(p.waypoints,function(q,r){n=n.add(r.element)})});return n}};$.fn[k]=function(n){if(c[n]){return c[n].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof n==="function"||!n){return c.init.apply(this,arguments)}else{if(typeof n==="object"){return c.init.apply(this,[null,n])}else{$.error("Method "+n+" does not exist on jQuery "+k)}}}};$.fn[k].defaults={continuous:true,offset:0,triggerOnce:false,context:i};$[m]=function(n){if(a[n]){return a[n].apply(this)}else{return a.aggregate()}};$[m].settings={resizeThrottle:200,scrollThrottle:100};e.load(function(){$[m]("refresh")})})(jQuery,"waypoint","waypoints",window);		

	/* ==================
	 * Toggle the Mobile Menu */
	menuLink.on('click', function( e ) { menuUL.slideToggle(); e.preventDefault(); })

	if ( responsive_viewport > 1024 ) { menuLink.css('display','none'); }

	/* ==================
	 * Multi-Toggle Dropdown */
	menuItem.on('click', function( e ) {
		
		var activeMenu 	= $(this).parent('li');
		
		collapse_menu = function() {
			activeMenu.removeClass('active');
			subMenu.removeClass('active');
		}

		open_menu = function( e ) {
			subMenu.removeClass('active');
			menu.removeClass('active');
			activeMenu.addClass('active');

			$('html').on('click', function( e ) {

				if ($(e.target).parents().index($('nav.menu')) == -1 ) {
					if ( $('nav.menu').is(':visible') ) {
						collapse_menu();
					}
				}
			});
		}

		if ( activeMenu.hasClass('active') ) { collapse_menu(); } else { open_menu(); }

	});

	/* ==================
	 * Convenient Links
	 * ================== */	
	$('.utility-menu > li').on('click', function(){
        $(this).toggleClass('active');
    });

    // To-Do: .drop-down to quick-links class
    
	/* ==================
	 * Sub-Menu Toggle */
	subMenuParent.on('click', function() {

		var activeSubMenu = $(this).parent('li.has-subnav').children('ul.children');		

		if ( activeSubMenu.hasClass('active') ) {
			
			activeSubMenu.removeClass('active');
		
		} else {

			activeSubMenu.addClass('active');

		}

	});	

	/* ==================
	 * Persistent Menu
	 * ================== */
	/* ==================
	 * If the user is on a very small screen (like a phone),
	 * then the main menu is fixed by default. For all
	 * larger devices we rely on waypoints to fix the menu
	 * when appropriate. */

	 if ( responsive_viewport > 481 ) {
	 
	    $('nav.menu').waypoint(function(event, direction) { 
	        
	        if ( direction === 'down' ) {
	            $(this).addClass('persistent persistent--top');
	        }

	        else {
	            $(this).removeClass('persistent persistent--top');
	        }
	    });
	 } //persistent menu

	/* ==================
	 * Portlet Sub Menu
	 * ================== */
	$('.widget_nav_menu a[href=#parent]').on('click', function() {
		
		if ( $(this).hasClass('active') ) {
			$(this).removeClass('active');
			$(this).next('.sub-menu').slideUp();
		} else {
			$(this).addClass('active');
			$(this).next('.sub-menu').css('position','relative').slideDown();
		}

	});

	tinsleyfied_menu_quick_search = function() {
		$('li.quick-search > a ').on('click', function( e ) {
			$(this).parent('li').html('<form name="hc_search" method="post" action="index.php?com=searchresult"><input type="search" name="hc_search_keyword" class="text-input form in-menu" placeholder="ex.: sharkey\'s storytime" speech x-webkit-speech></form>');
			e.preventDefault();
		});
	}

	tinsleyfied_menu_quick_search();

} // tinsleyfied_menu()

/* ==================
 * Init!
 * ================== */
tinsleyfied_menu();

if (responsive_viewport < 481) {


} /* end smallest screen */

if (responsive_viewport <= 481 ) {

	//$('nav.menu').after('<nav id=springboard><a href="http://sherman.library.nova.edu/m/research.php" class="icon-book" title="">Research</a><a href="http://sherman.library.nova.edu/heliosdev" class="icon-calendar" title="">Events</a><a href="http://sherman.library.nova.edu/rooms/" class="icon-key" title="">Rooms</a><a href="http://sherman.library.nova.edu/m/directions.php" class="icon-map" title="">Directions</a><a href="http://sherman.library.nova.edu/m/hours.php" class="icon-clock" title="">Hours</a></nav>');

}


/* if is larger than 481px */
//if (responsive_viewport > 481) {} /* end larger than 481px */
//if (responsive_viewport < 768) {}
/* if is above or equal to 768px */
//if (responsive_viewport >= 768) {}

/* off the bat large screen actions */
if (responsive_viewport > 1030) {}

if ( $('html').hasClass('lt-ie9') ) {

	/* ==================
 	 * Politely prompt patrons using IE8 to upgrade or try new, auto-updating browsers 
 	 */
 	old_ie_prompt = function() {
 		$('nav[role=navigation]').after('<!--[if IE 8]><section class="alert alert--fixed alert--note" style="margin-bottom:14px;"><p class="wrap"><span class="icon-ie"></span> Did you know that your version of <b>Internet Explorer</b> is several years old? Give <a href="http://www.firefox.com" target="new" class="icon-firefox"> Firefox</a>, <a href="http://www.google.com/chrome" target="new" class="icon-chrome"> Google Chrome</a>, or <a href="http://www.apple.com/safari" class="icon-safari"> Safari</a> a try.</p></section><![endif]-->');
 	}

	verify_subnet = function() {
		
		// Temporarily, we want to prevent this notice when patrons are using in-house computers.
		// We'll get away with this by matching their IPs to our subnet.

		$.getJSON('http://systems.library.nova.edu/feeds/getip.php?callback=?', 
			function( data ) {

				var address 	= data.ip,
					octets		= address.split('.'),
					subnet 		= octets[0] + octets[1];

					if ( subnet != 13752 ) {

						old_ie_prompt();

					}

			});

	}

	verify_subnet();   


}

/* ==================
 * Approximate screen size without duplicating breakpoints
 * using conditional css (// http://adactio.com/journal/5429/) 
 * NOTE: This didn't seem to work. */

//var size = window.getComputedStyle(document.body,':after').getPropertyValue('content');