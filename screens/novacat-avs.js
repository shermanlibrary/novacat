/*
 * Support for srchhelp_X:
 *
 */
/*
 * support for "clear form" button:
 * note: the form name and the element names correspond to the js in
 * websrchhelp.c:srchhelp_x_advancesearchformbegin().
 * form.reset() does not work like we would like... have to
 * explicitly clear the fields.
 */
function iiiDoReset_1()
{
obj = getObj("search");
/* if (obj) obj.reset(); C987846 */

for (var i = 0; i < obj.elements.length; i++)
	{
	var x = obj.elements[i];
	if (!x.name) continue;
	if (x.name.substr(0,10) == "fieldLimit") x.selectedIndex = 0;
	else if (x.name.substr(0,7) == "boolean") x.selectedIndex = 0;
	else if (x.name == "SORT") x.selectedIndex = 0;
	else if (x.name == "sortdropdown") x.selectedIndex = 0;
	else if (x.name == "searchscope")
			x.selectedIndex = (savedScopeIndex - 1);
	else if (x.type == "text") x.value = "";
	else if (x.type == "select-one") x.selectedIndex = 0; /* C987846 */
	else if (x.type == "select-multiple") x.selectedIndex = 0; /* C987846 */
	else if (x.type == "checkbox") x.checked = false;
	}
}
//method of AdvancedSearchForm is called by the createSearchString
//and createLimitString methods
//utf8 encodes values to be passed to webpac
function prepHTMLValue(value)
{
if(value != "" )
	{
	newvalue = encodeURIComponent(value);
	escvalue = escape(value);

	if(escvalue.indexOf("%u") != -1 && encode != "UTF-8")
		return value;
	else
		return newvalue;
	}
return "";
}
//method of AdvancedSearchForm creates the search string for the search request
function createSearchString()
{
//start at one because the arrays are created by extracting the numeric portion of the form element's name
//for example: searchText1, searchText2
for(var i=1; i < this.searchTextArray.length; i++)
	{
	var searchVal = this.prepHTMLValue(this.getValue(this.searchTextArray[i]));
	var searchSlice = this.getValue(this.fieldLimitArray[i]);
	if( searchVal != "" )
		{
		if(i != 1 )
			{
			this.searchString +=this.getValue(this.booleanArray[i-1]);
			}
		this.searchString += searchSlice+"("+searchVal+")";
		}
	}
}
//method of AdvancedSearchForm creates the limit string for the search request
function createLimitString()
{
for( var i in this.limitArray)
	{
	var elemName = new String(this.limitArray[i].name);
	var elemValue = this.getValue(this.limitArray[i]);
	if( elemValue != "" )
		{
		if(elemName.indexOf("Db") != -1 || elemName.indexOf("Da") != -1)
			{
			this.limitString += "&"+elemName+"="+this.prepHTMLValue(elemValue);
			}
		else
			{
			this.limitString += "&"+elemName+"="+elemValue;
			}
		}
	}
}
//AdvancedSearchFrom is a javascript object
function AdvancedSearchForm(form)
{
//methods
this.getValue = getValue;
this.prepHTMLValue = prepHTMLValue;
this.createSearchString = createSearchString;
this.createLimitString = createLimitString;

//members
//	initialize limit members
this.limitString = new String("");
this.limitArray = new Array();
this.limitArray.scope = ( form.elements["searchscope"] ) ? form.elements["searchscope"] : "";
this.limitArray.dateafter = (form.elements["Da"]) ? form.elements["Da"] : "";
this.limitArray.datebefore = (form.elements["Db"]) ? form.elements["Db"] : "";
this.limitArray.sortby =  (form.elements["SORT"]) ? form.elements["SORT"] : "";
this.limitArray.limitavail =  (form.elements["availlim"]) ? form.elements["availlim"] : "";
this.limitArray.circhistlimit =  (form.elements["circhistlimit"]) ? form.elements["circhistlimit"] : "";

//initialize rest of limitArray - webpac creates the next func at runtime
AdvancedSearchForm_initLimits(this, form);

//	initialize search members
this.searchString = new String("");
this.fieldLimitArray = new Array();
this.searchTextArray = new Array();
this.booleanArray = new Array();
this.fieldPattern = /fieldLimit(\d+)/;
this.searchPattern = /searchText(\d+)/;
this.booleanPattern = /boolean(\d+)/;
for(var i=0; i < form.elements.length; i++)
	{
	if (!form.elements[i].name) continue;
	if( form.elements[i].name.indexOf("fieldLimit") != -1)
		{
		var matches = this.fieldPattern.exec(form.elements[i].name)
		this.fieldLimitArray[matches[1]] = form.elements[i];
		}
	if( form.elements[i].name.indexOf("searchText") != -1)
		{
		var matches = this.searchPattern.exec(form.elements[i].name)
		this.searchTextArray[matches[1]] = form.elements[i];
		}
	if( form.elements[i].name.indexOf("boolean") != -1)
		{
		var matches = this.booleanPattern.exec(form.elements[i].name)
		this.booleanArray[matches[1]] = form.elements[i];
		}
	}
}
function submitSearch(form, pathname, use_nosrch)
{
//create a new AdvancedSearchForm with the form as the parameter
var thisForm = new AdvancedSearchForm(form);

thisForm.createLimitString();
thisForm.createSearchString();
var nosrchstring = "SEARCH";
var subkeystring = "";

if (use_nosrch)
	{
	nosrchstring = "NOSRCH";
	subkeystring = "&SUBKEY=" . thisForm.searchString;
	}

form.action = location.protocol+ "//" + location.host + pathname
	+ "?" + nosrchstring + "=" +  thisForm.searchString
	+ thisForm.limitString + subkeystring;

window.location.href = location.protocol+ "//" + location.host + pathname
	+ "?" + nosrchstring + "=" +  thisForm.searchString
	+ thisForm.limitString + subkeystring;

//return false because we're sending the form using window.location.href. returning true will send the form in the traditional way
return false;
}
function strip_surrounding_parens(buf)
/*
 * strip the leading and trailing parentheses from the search string
 * only if they balance each other, e.g.:
 * (apple pie) ==> apple pie
 *   MODIFIED because the trailing paren balances the trailing paren
 * (apple pie) and (peach cobbler) ==> (apple pie) and (peach cobbler)
 *   NOT MODIFIED because the paren after "pie" balances the leading paren
 * ((apple pie) and (peach cobbler)) ==> (apple pie) and (peach cobbler)
 *   MODIFIED because the trailing paren balances the trailing paren
 *
 * Compare to c version in httpuchars.c:strip_surrounding_parens()
 * for interesting reading.
 */
{
var p;
var n;

if (buf.substring(0, 1) != "(" ||
	buf.substring(buf.length - 1, buf.length) != ")")
	{
    return buf; /* no leading and trailing parentheses == no modification */
	}

for (n = 0, p = 0; p < buf.length; p++)
    if (buf.substring(p, p + 1) == "(") n++;
    else if (buf.substring(p, p + 1) == ")")
        {
        if (!(--n) && p < buf.length - 1)
			{
            return buf;  /* matching paren not at end == no modification */
			}
        }
/* made it through the loop but might have unbalanced parens */
if (n) return buf; /* unbalanced parentheses - no modification */

/* return without leading and trailing parens */
return buf.substring(1, buf.length - 1);
}
//function used to populate the form when modifySearch is requested
//if SUBKEY is not present we will fall back to using SEARCH, which is
//the query in raw format (this fallback is necessary for the case when the
//request has "no entries found").  If neither arg is in the string, we
//will alert user that search information could not be processed
function modifySearch(string)
{
var form = document.search;
if (!form) return; /* it is OK if the form is not on the page */
var modifyString = string;

//grab utf-8 encoded SUBKEY field and remove it from the string
if( modifyString.indexOf("&SUBKEY=") == -1 )
	{
	if (modifyString.indexOf("?SEARCH=") == -1)
	    {
	    if( modifyString.indexOf("searcharg=") == -1 )
           	{
				/* no prior search term - not an error */
                return;
	        }
	    else
		{
		var searchPattern = /searcharg=([^&]+)/;
		}
	    }
	else
	    {
	    var searchPattern = /\\?SEARCH=([^&]+)/;
	    }
	}
else
	{
	var searchPattern = /&SUBKEY=([^&]+)/;
	}

var searchTextArray = searchPattern.exec(modifyString);
var searchText = searchTextArray[1].replace(/\+/g," ");
modifyString = modifyString.replace(/&SUBKEY=(.+)&{0,1}/,"");

var modifyArray = modifyString.split("&");

//remove the first element from the array leaving only limit fields in the
//modify array
//the first element is the original search term and will display diacritics
//which we can't process on the client-side
modifyArray.shift();

