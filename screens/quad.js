/* ==================
 * Set Error Cookie
 * ================== */
/* ==================
 * We will set a cookie that remembers which login
 * box submitted before the page reloads.
 - 1. If the cookie doesn't exist, create it.
 - 2. When a login box is submitted, store
 - 	  its id, erasing the previous cookie.
 */

if ( $.cookie('nc_login_error') ) {
	
	var errorForm	= $.cookie('nc_login_error');
	/* ==================
	 * Potential Values:
	 - 1. error-free
	 - 2. #sharkform
	 - 3. #browardform
	 - 4. #unsform
	 - 5. #hpdform
	 */

} else {

	$.cookie('nc_login_error', 'error-free');

}

/* ==================
 * Up Front Variables
 * ================== */
var logins			=	$('div.login input[type=text]'),
	systemError		=	$('#system-error').text(),
	defaultLogin	=	$('#sharkform');


/* ==================
 * $Error Message Display
 * ================== */
/* ==================
 * If there is a login error, then
 * store then hide the original message, making it
 * append to the relevant login box. 
 */
login_error_message = function( errorForm, errorMessage ) {
    
    if ( systemError != '' && errorForm != 'error-free' ) {
	 	
    	if ( errorForm == '#sharkform' ) {

    		errorMessage = 'We are unable to identify you with the SharkLink username you entered. Please try again. <a href="#" onClick="winhelp1(); return false;" style="color: white; text-decoration: underline;">Need Help?</a>';

    	} else if ( errorForm == '#browardform') {
    		errorMessage = 'We are unable to identify you with the library card number you entered. Please try again. <a href="#" onClick="winhelp2(); return false;" style="color: white; text-decoration: underline;">Need Help?</a>';
    	} else if ( errorForm == '#hpdform' ) {
    		errorMessage = 'We are unable to identify you with the NSU ID / HPD Library ID number that you entered. Please try again. <a href="#" onClick="winhelp3(); return false;" style="color: white; text-decoration: underline;">Need Help?</a>';
    	} else if ( errorForm == "#unsform") {
    		errorMessage = 'We are unable to identify you with the University School ID that you entered. Please try again. <a href="#" onClick="winhelp4(); return false;" style="color: white; text-decoration: underline;">Need Help?</a>';
    	} else {

	 		errorMessage = systemError;
	 	}
    }

    if ( errorMessage ) {
	    var inactiveLogin   = $('div.login').not( errorForm );
	    
	    $( errorForm ).append('<p id="errorText" style="display:none;">' + errorMessage + '</p>');

	    $( '#errorText' ).slideDown();

	    inactiveLogin
	        .animate({
	          opacity: 0.5
	          }, 600);
        }
}

/* ==================
 * $Fade
 * ================== */
/* ==================
 * 1. Autofocus the SharkLink Screen
 * 2. Fade the Others
 */

a_gentler_login_interface = function() {

  defaultLogin.find('input[type=text]').focus();
    $('.login').not( defaultLogin ).animate({
    opacity: 0.5
  }, 600);

  logins.on('focus', function() {
    var activeLogin = $(this).parents('.login');

    activeLogin.animate({
      opacity: 1
    }, 600);
    
    $('.login').not(activeLogin).animate({
      opacity: 0.5
      }, 600);

  });
}

/* ==================
 * Init!
 * ================== */

login_error_message( errorForm );
//a_gentler_login_interface();

/* ==================
 * $Keyboard Enabled
 * ================== */
$(document).ready(function() {
	$('#novacatpw1').keydown(function(e) {
		if(e.keyCode == 13)
		{
	  		//$('#submit1').focus();
			//$('#submit1').click();
		}
	});

	$('#novacatpw2').keydown(function(e) {
		if(e.keyCode == 13)
		{
	  		$('#submit4').focus();
			$('#submit4').click();
		}
	});

	$('#novacatpw3').keydown(function(e) {
		if(e.keyCode == 13)
		{
	  		$('#submit3').focus();
			$('#submit3').click();
		}
	});

	$('#novacatpw4').keydown(function(e) {
		if(e.keyCode == 13)
		{
	  		$('#submit2').focus();
			$('#submit2').click();
		}
	});
});

/* ==================
 * Form Validation
 * ================== */
/* ==================
 - 1. Get all of the input values, store cookie.
 - 2. Plug the cookie into a variable. 
 - 3. Some math.
 */
function validate_all(form, obj)
{

	var arr_idpw 	= getInputValues( form ),
		arr_idpwchk = new Array(),
		formHash	= $(obj).data('error');
	
	$.cookie('nc_login_error', formHash);

	for(i = 0; i < arr_idpw.length; i++)
	{
		if(arr_idpw[i][0] != "")
		{
			arr_idpwchk[arr_idpwchk.length] = i;
		}
		if(arr_idpw[i][1] != "")
		{
			arr_idpwchk[arr_idpwchk.length] = i;
		}
	}


	errchk =  inputCheck(arr_idpwchk, obj);
	
	if(!errchk)
	{
		var params = new Array();
		switch(arr_idpwchk[0])
		{
		case 0:
			params["extpatid"] = arr_idpw[arr_idpwchk[0]][0];
			params["extpatpw"] = arr_idpw[arr_idpwchk[0]][1];
			params["extpatserver"] = "student";
			break;
		case 1:
			params["extpatid"] = arr_idpw[arr_idpwchk[0]][0];
			params["extpatpw"] = arr_idpw[arr_idpwchk[0]][1];
			params["extpatserver"] = "staff";
			break;
		case 2:
			params["name"] = arr_idpw[arr_idpwchk[0]][0];
			params["code"] = arr_idpw[arr_idpwchk[0]][1];
			break;
		case 3:
			params["name"] = arr_idpw[arr_idpwchk[0]][0];
			params["code"] = arr_idpw[arr_idpwchk[0]][1];
			break;
			
		}
		post_to_url("", params);
	}
	
}

function clearValues()
{
	
	$('input[type=text]').val('');

}

function getInputValues(form)
{
	$('.login-error').html('');

	var novacatid1 = trim(form.novacatid1.value);
	var novacatpw1 = trim(form.novacatpw1.value);

	var novacatid2 = trim(form.novacatid2.value);
	var novacatpw2 = trim(form.novacatpw2.value);

	var novacatid3 = trim(form.novacatid3.value);
	var novacatpw3 = trim(form.novacatpw3.value);

	var novacatid4 = trim(form.novacatid4.value);
	var novacatpw4 = trim(form.novacatpw4.value);


	var arr_idpw = new Array(4);
	arr_idpw[0] = new Array(2);
	arr_idpw[1] = new Array(2);
	arr_idpw[2] = new Array(2);
	arr_idpw[3] = new Array(2);

	arr_idpw[0][0] = novacatid1;
	arr_idpw[0][1] = novacatpw1;

	arr_idpw[1][0] = novacatid2;
	arr_idpw[1][1] = novacatpw2;

	arr_idpw[2][0] = novacatid3;
	arr_idpw[2][1] = novacatpw3;

	arr_idpw[3][0] = novacatid4;
	arr_idpw[3][1] = novacatpw4;
	
	return arr_idpw;
}

function inputCheck(arr_idpwchk, obj)
{
	errchk = false;

	if(arr_idpwchk.length != 2)
	{
		errchk = true;
		if(arr_idpwchk.length == 0)
		{
			$('#login-error').html("<p class='login-error h3'>Please enter a valid username and password or number. Please try again or click <a href='javascript:winhelp();'>here</a> for assistance.</p>");
		}
		else if(arr_idpwchk.length == 1)
		{

			switch(arr_idpwchk[0])
			{

			case 0:
				
				$.cookie('nc_login_error', '#sharkform');
				var errorMessage = "Unable to identify you with the Shark Link username and password that you entered. Please try again or click <a href='javascript:winhelp1();'>here</a> for assistance.";
				
				// Trigger the Alert
				login_error_message( $.cookie('nc_login_error'), errorMessage );				

			break;

			case 1:

				$.cookie('nc_login_error', '#unsform');
				var errorMessage = "Unable to identify you with the U-School Student login and password that you entered. Please try again or click <a href='javascript:winhelp4();'>here</a> for assistance.";

				// Trigger the Alert
				login_error_message( $.cookie('nc_login_error'), errorMessage );		
			break;

			case 2:

				$.cookie('nc_login_error', '#hpdform');
				errorMessage = "Unable to identify you with the last name and NSU ID/HPD Library ID number that you entered. Please try again or click <a href='javascript:winhelp3();'>here</a> for assistance.";
				
				// Trigger the Alert
				login_error_message( $.cookie('nc_login_error'), errorMessage );		

			break;

			case 3:

				$.cookie('nc_login_error', '#browardform');
				errorMessage = "Unable to identify you with the last name and library card number that you entered. Please try again or click here for assistance. <a href='javascript:winhelp2();'>here</a> for assistance.";

				// Trigger the Alert
				login_error_message( $.cookie('nc_login_error'), errorMessage );				
			break;
			}

		}
		else if(arr_idpwchk.length > 2)
		{
			
			$('#login-error').html("<p class='login-error h3'>Only use one form when attempting to login. Please try again or click <a href='javascript:winhelp();'>here</a> for assistance.</p>");
			//$('#login-error').fadeIn("slow");
			//document.getElementById("noerrmsg").innerHTML = "Only use one form when attempting to login. Please try again or click <a href='javascript:winhelp();'>here</a> for assistance. ";
			$('#novacatid1').focus();
			clearValues();
		}		

	}
	else
	{
		if(arr_idpwchk[0] != arr_idpwchk[1])
		{
			//$('#login-error').fadeIn().delay(3000).fadeOut('slow');
			
			//document.getElementById("noerrmsg").innerHTML = "Only use one form when attempting to login. Please try again or click <a href='javascript:winhelp();'>here</a> for assistance.";
			$('#login-error').html("<p class='login-error h3'>Only use one form when attempting to login. Please try again or click <a href='javascript:winhelp();'>here</a> for assistance.</p>");
			$('#novacatid1').focus();
			clearValues();
			errchk = true;
		}
		else
		{
			//if clicked wrong submit button
			switch(arr_idpwchk[0])
			{
				case 0:
				if(obj.id != "submit1")
				errchk = true;
				break;
				case 1:
				if(obj.id != "submit4")
				errchk = true;
				break;
				case 2:
				if(obj.id != "submit3")
				errchk = true;
				break;
				case 3:
				if(obj.id != "submit2")
				errchk = true;
				break;
				
			}
			if(errchk)
			{
				$('#login-error').html("<p class='login-error h3'>Please click on the submit button below where you entered your information.</p>");
			}
			else
			{
				$('#login-error').html('');
			}
		}
	}

	return errchk;
}

function post_to_url(path, params, method) 
{ 
    method = method || "post"; // Set method to post by default, if not specified. 
 
    // The rest of this code assumes you are not using a library. 
    // It can be made less wordy if you use one. 
    var form = document.createElement("form"); 
    form.setAttribute("method", method); 
    form.setAttribute("action", path); 
 
    for(var key in params) 
   { 
        var hiddenField = document.createElement("input"); 
        hiddenField.setAttribute("type", "hidden"); 
        hiddenField.setAttribute("name", key); 
        hiddenField.setAttribute("value", params[key]); 

        form.appendChild(hiddenField); 
    } 
 
    document.body.appendChild(form);    // Not entirely sure if this is necessary 
    form.submit(); 
}

function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
}

// Removes ending whitespaces -- local FN
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}

// Removes leading and ending whitespaces -- local FN
function trim( value ) {
	
	return LTrim(RTrim(value));
}

function setOnfocus(obj){
	switch(obj.id)
	{
		case "novacatpw1":
		$("#submit1").focus();
		break;
		case "novacatpw2":
		$("#submit4").focus();
		break;
		case "novacatpw3":
		$("#submit3").focus();
		break;
		case "novacatpw4":
		$("#submit2").focus();
		break;
	}
}

function captureEnterKey(obj, e){
	var key;
	if(window.event)
	{
		key = window.event.keyCode;
	}
	else
	{
		key = e.which;
	}
	if (key == 13)
	{
		switch(obj.id)
		{
			case "novacatpw1":
				document.forms[0].submit1.focus();
				document.forms[0].submit1.click();
				//$('#submit1').focus();
				//$('#submit1').click();
				break;
			case "novacatpw2":
				document.forms[0].submit4.focus();
				document.forms[0].submit4.click();
				//$('#submit4').focus();
				//$('#submit4').click();
				break;
			case "novacatpw3":
				document.forms[0].submit3.focus();
				document.forms[0].submit3.click();
				//$('#submit3').focus();
				//$('#submit3').click();
				break;
			case "novacatpw4":
				$('#submit1').blur();
				document.forms[0].submit2.focus();
				document.forms[0].submit2.click();
				//$('#submit2').focus();
				//$('#submit2').click();
				break;
		}
	}
}

function winhelp() {
	window.open('/screens/pverify_help.html','winhelp','width=400,height=460,scrollbars=yes')
}
	
function winhelp1() {
	window.open('/screens/pverify_help1.html','winhelp1','width=400,height=460,scrollbars=yes')
}
	
function winhelp2() {
	window.open('/screens/pverify_help2.html','winhelp2','width=400,height=460,scrollbars=yes')
}
	
function winhelp3() {
	window.open('/screens/pverify_help3.html','winhelp3','width=400,height=460,scrollbars=yes')
}
	
function winhelp4() {
	window.open('/screens/pverify_help4.html','winhelp4','width=400,height=460,scrollbars=yes')
}
	
